interface ImportMeta {
  env: {
    VUE_APP_SUPABASE_URL: string;
    VUE_APP_SUPABASE_ANON_KEY: string;
  };
}
