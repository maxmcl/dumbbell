import { InjectionKey } from "vue";
import { createStore, useStore as baseUseStore, Store } from "vuex";

export type StrengthExercise = {
  id: string;
  name: string;
  sets: number;
  reps: number;
  weight: number;
};

export type CardioExercise = {
  id: string;
  type: string;
  distance: number;
  duration: number;
  pace: number;
};

type Workout = {
  name: string;
  type: string;
  exercises: Array<StrengthExercise | CardioExercise>;
};

export interface State {
  user: string | null;
  workouts: Array<Workout>;
}

export const key: InjectionKey<Store<State>> = Symbol();

export const store = createStore<State>({
  state: {
    user: null,
    workouts: [],
  },
  mutations: {
    setUser(state, user) {
      console.log(`Setting user =`);
      console.log(user);
      state.user = user;
    },
    addWorkout(state, workout: Workout) {
      console.log("Adding workout=");
      console.log(workout);
      state.workouts.push(workout);
    },
  },
});

export function useStore() {
  return baseUseStore(key);
}
