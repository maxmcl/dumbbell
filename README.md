# dumbbell
[Credit](https://www.youtube.com/watch?v=3tF0fGkd4ho)

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


## Annoying ESLint nvim linting issue
Maybe the better fix (2nd line)?
```bash
$ cat .vim/coc-settings.json 
{
  "typescript.format.insertSpaceAfterOpeningAndBeforeClosingNonemptyBraces": true,
  "eslint.format.enable": true
}
```
